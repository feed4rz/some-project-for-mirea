import dotenv from 'dotenv'
import express from 'express'

dotenv.config()

const app = express()

app.get('/hello', async (req, res) => {
	res.send(', world!')
})

app.listen(8080)
