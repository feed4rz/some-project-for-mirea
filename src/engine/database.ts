import { Pool } from 'pg'

export const pool = new Pool({
	connectionString: process.env.POSTGRES_URL,
	max: 300
})

pool.on('error', (err) => {
	console.error('Unexpected error on idle client', err)
	process.exit(-1)
})

export async function tx(callback) {
	const client = await pool.connect()
	await client.query('BEGIN')

	let result

	try {
		result = await callback(client)
		await client.query('COMMIT')
	} catch (e) {
		await client.query('ROLLBACK')

		throw e
	} finally {
		client.release()
	}

	return result
}
